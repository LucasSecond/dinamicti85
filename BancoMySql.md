<!-- create database dinamico85db;

use dinamico85db;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `login` varchar(100) NOT NULL,
  `senha` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--



-- --------------------------------------------------------


-- --------------------------------------------------------

--
-- Estrutura da tabela `administrador`
--

CREATE TABLE `administrador` (
  `id` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `login` varchar(100) NOT NULL,
  `senha` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `administrador`
--

INSERT INTO `administrador` (`id`, `nome`, `email`, `login`, `senha`) VALUES
(1, 'Lucas Henrique', 'lucas@dev.shellcode', 'lucas', '123456');

-- --------------------------------------------------------

--
-- Estrutura da tabela `banner`
--

CREATE TABLE `banner` (
  `id_banner` int(11) NOT NULL,
  `titulo_banner` varchar(255) NOT NULL,
  `link_banner` varchar(255) NOT NULL,
  `img_banner` varchar(150) NOT NULL,
  `alt` varchar(255) NOT NULL,
  `banner_ativo` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL,
  `categoria` varchar(150) NOT NULL,
  `cat_ativo` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticias`
--

CREATE TABLE `noticias` (
  `id_noticia` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `titulo_noticia` varchar(255) NOT NULL,
  `img_noticia` varchar(100) NOT NULL,
  `visita_noticia` int(11) NOT NULL,
  `data_noticia` date NOT NULL,
  `noticia_ativo` varchar(1) NOT NULL,
  `noticia` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `post`
--

CREATE TABLE `post` (
  `id_post` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `titulo_post` varchar(250) NOT NULL,
  `descricao_post` text NOT NULL,
  `img_post` varchar(200) NOT NULL,
  `visitas` int(11) NOT NULL,
  `data_post` date NOT NULL,
  `post_ativo` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id_banner`);

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indexes for table `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id_noticia`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id_post`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id_banner` int(11) NOT NULL AUTO_INCREMENT;
select * from banner;
--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT;

select * from categoria;
--
-- AUTO_INCREMENT for table `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id_noticia` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id_post` int(11) NOT NULL AUTO_INCREMENT;


/*=================================================================================*/

/*Delimiters*/

/*================= Administrador ====================================================*/
DROP PROCEDURE 'sp_insert_administrador'

DELIMITER $$
CREATE PROCEDURE `sp_insert_administrador`
(
_nome varchar(200),
_email varchar(200),
_login varchar(100),
_senha varchar(90)
)
BEGIN
	insert into administrador (nome, email, login, senha) values (_nome, _email,_login, _senha);
    select * from administrador where id = (select @@identity);
END $$

CALL sp_insert_administrador('Kaue Emanuel', 'BotTiozao@dev.com','Tiozao','botzeira');
select * from administrador;
  
  /*================= Categoria ====================================================*/
DROP PROCEDURE ´sp_categoria_insert´

DELIMITER $$
CREATE PROCEDURE `sp_categoria_insert`
  (
  _categoria varchar(100),
  _cat_ativo varchar(1)
  )
  BEGIN	
	insert into categoria (categoria,cat_ativo)
	values (_categoria,_cat_ativo);
	 select * from categoria where id = (select @@identity);
  END $$
  
  CALL sp_categoria_insert('Teste','0');
  select * from categoria;
  select * from administrador;

/*================= Banner ====================================================*/
  
  
/*================= Notícias ====================================================*/
DROP PROCEDURE ´sp_noticias_insert´

DELIMITER $$
CREATE PROCEDURE `sp_noticias_insert`
  (
  _titulo varchar(255),
  _img varchar (100),
  _visita int(11),
  _data date,
  _noticia_ativo varchar(1),
  _noticia text
  )
  BEGIN
	insert into noticias (titulo_noticia, img_noticia, visita_noticia, data_noticia, noticia_ativo, noticia)
	values (_titulo, _img, _visita,_data, _noticia_ativo, _noticia);
	 select * from noticias where id = (select @@identity);
  END $$
  
  CALL sp_noticias_insert('Pão de Queijo','Pão', '1K', NOW(),'1','Good');
  select * from noticias;
  
/*================= Usuário ====================================================*/
  DROP PROCEDURE ´sp_noticias_insert´;

DELIMITER $$
CREATE PROCEDURE `sp_usuario_cadastro`
  (
  _nome varchar (50),
  _email varchar(50),
  _foto varchar(100),
  _login varchar(100),
  _senha varchar(90)
  )
  BEGIN
	insert into usuario (nome, email, foto, login, senha)
	values (_nome, _email, _foto, _login, _senha);
	 select * from usuario where id = (select @@identity);
  END $$
  
  CALL sp_usuario_insert('Kleber Viera','Kleber@lol.com.br','Kleber', '133456');
  select * from usuario;
  
  INSERT INTO `usuario` (`id`, `nome`, `email`, `login`, `senha`) VALUES
(0, 'Kleber Vieira', 'kleber@lol.com.br', 'Kleber', '123456');
  
/*==============================================================================================*/
  
create table usuario
(
	id int not null auto_increment,
    nome varchar(50) not null,
    email varchar(50),
    foto varchar(100) not null,
    primary key (id)
  );
  
  select * from usuario;
  select * from administrador;
  
COMMIT; -->
