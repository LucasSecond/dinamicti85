<?php
//inicializar a sessão de usuário
if (!isset($_SESSION)){
    session_start();
}

//definindo padrão de Zona GMT (TimeZone) -3,00
date_default_timezone_set('America/Sao_Paulo');

//inicia carregamento das classes do projeto
spl_autoload_register(function($nome_classes)
{
    $server_str = $_SERVER['REQUEST_URI'];
    //Essa linha de baixo carrega as classes da pasta "admin" onde contém: 
    //Administrador, categoria, notícia e sql.
    $caminho = (strpos($server_str, 'admin') !==false)?'class':'admin/class';
    // Na linha de baixo carrega a classe dentro da pasta user, que no caso seria sql e usuário.
    // $caminho = (strpos($server_str, 'user') !==false)?'class':'admin/user/class';
    $nome_arquivo = $caminho.DIRECTORY_SEPARATOR.$nome_classes.".php";
    if(file_exists($nome_arquivo))
    {
        require_once($nome_arquivo);
}

});

//Criar constantes do servidor de banco de dados
define ('IP_SERVER_DB', '127.0.0.1');
// define ('HOSTNAME','ITQ0626039W10-1');
define ('HOSTNAME','PC0-1');
define ('NOME_BANCO','dinamico85db');
define ('USER_DB','root');
define ('PASS_DB','');
?>