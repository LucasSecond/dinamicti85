<?php
require_once('conexao.php');
$query = "select * from noticia";
$cmd = $cn->prepare($query);
$cmd->execute();
$noticias_retornadas = $cmd->fetchAll(PDO::FETCH_ASSOC);
if(count($noticias_retornadas)>0) {
    print_r($noticias_retornadas);
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <title>Lista de notícias</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    
    <table id="tb_categoria" width="100%" border="0" cellpadding="1" bgcolor="#fff">
        <tr bgcolor="#993300" align="center">
            <th width="15%" height="2"><font size="2" color="#fff">Cod. Noticia</font></th>
            <th width="57%" height="2"><font size="2" color="#fff">Cod. Categoria</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Titulo</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Visitas</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Data</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Ativo</font></th>
            <th colspan="2"><font size="2" color="#fff">Opções</font></th>
        </tr>

    <?php
        require_once('../config.php');
        $notici_retornadas = Noticia::getList();
        foreach($noticias_retornadas as $noticia) {
    ?>

        <tr align="center">
            <td><font size="2" face="verdana, arial"color="#000"><?php echo $noticia['id_noticia']; ?></font></td>
            <td><font size="2" face="verdana, arial"color="#000"><?php echo $categoria['id_categoria']; ?></font></td>
            <td><font size="2" face="verdana, arial"color="#000"><?php echo $noticia['titulo_noticia']; ?></font></td>
            <td><font size="2" face="verdana, arial"color="#000"><?php echo $noticia['img_noticia']; ?></font></td>
            <td><font size="2" face="verdana, arial"color="#000"><?php echo $noticia['visita_noticia']; ?></font></td>
            <td><font size="2" face="verdana, arial"color="#000"><?php echo $noticia['data_noticia']; ?></font></td>
            <td><font size="2" face="verdana, arial"color="#000"><?php echo $noticia['noticia_ativo']; ?></font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#fff"><a href="principal.php">Alterar</a></font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#fff"><a href="principal.php">Excluir</a></font></td>
        </tr>
<?php }} ?>
</table>

</body>
</html>