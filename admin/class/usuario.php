<?php

class Usuario
{
    // ! Atributos da classe usuário
    private $id;
    private $nome;
    private $email;
    private $login;
    private $senha;
    private $foto;

    // ! Método de acesso - Getters and Setters
    // ! ID
    public function getId()
    {
        return $this->id;
    }
    public function setId($value)
    {
        $this->id = $value;
    }

    // ! Nome
    public function getNome()
    {
        return $this->nome;
    }
    public function setNome($value)
    {
        $this->nome = $value;
    }

    // ! Email
    public function getEmail()
    {
        return $this->email;
    }
    public function setEmail($value)
    {
        $this->email = $value;
    }

    // ! Login
    public function getLogin()
    {
        return $this->login;
    }
    public function setLogin($value)
    {
        $this->login = $value;
    }

    // ! Senha
    public function getSenha()
    {
        return $this->senha;
    }
    public function setSenha($value)
    {
        $this->senha = $value;
    }

    // ! Foto
    public function getFoto()
    {
        return $this->senha;
    }
    public function setFoto($value)
    {
        $this->foto = $value;
    }

// ==================================================================================================================================== //
    
    // ! Método - Função de efetuar login
    public function efetuarLogin($_login,$senha_cript)
    {
        $sql = new Sql();
        $senha_cript = md5($senha_cript);
        $results = $sql->select("SELECT * FROM usuario WHERE login = :login and senha = :senha",
        array(
            ':login'=>$_login,
            ':senha'=>$senha_cript)
            );
        if(count($results)>0)
        {
            $this->setData($results[0]);
        }
    }

    // ! Método - Função de inserção de novo usuário
    public function insert($_nome,$_email,$_foto,$_login,$_senha)
    {
        $sql = new Sql();
        $results = $sql->select("call sp_usuario_cadastro(':nome, :email, :login, :senha, :foto)",
        array(
            ":nome"=>$_nome,
            ":email"=>$_email,
            ":login"=>$_login,
            ":senha"=>md5($_senha),
            ":foto"=>$_foto
            )
        );
        if(count($results)>0)
        {
            $this->setData($results[0]);
        }
    }

    // ! Método construtor
    public function _construct($_id, $_nome="",$_email="",$_login="",$_senha="",$_foto="")
    {
        $this->nome =$_id;
        $this->nome =$_nome;
        $this->email =$_email;
        $this->login =$_login;
        $this->senha =$_senha;
        $this->foto =$_foto;
    }

    // ! Método - Data
    public function Data($data)
    {
        $this->setId($data['id']);
        $this->setNome($data['nome']);
        $this->setEmail($data['email']);
        $this->setLogin($data['login']);
        $this->setSenha($data['senha']);
        $this->setFoto($data['foto']);
    }
}
?>

?>