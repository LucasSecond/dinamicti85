<?php

class Administrador
{
    //Atributos da classe administrador
    private $id;
    private $nome;
    private $email;
    private $login;
    private $senha;

    //Método de acesso - Getters and Setters - Administrador
    // ID
    public function getId()
    {
        return $this->id;
    }
    public function setId($value)
    {
        $this->id = $value;
    }

    // Nome
    public function getNome()
    {
        return $this->nome;
    }
    public function setNome($value)
    {
        $this->nome = $value;
    }

    // Email
    public function getEmail()
    {
        return $this->email;
    }
    public function setEmail($value)
    {
        $this->email = $value;
    }

    // Login
    public function getLogin()
    {
        return $this->login;
    }
    public function setLogin($value)
    {
        $this->login = $value;
    }

    // Senha
    public function getSenha()
    {
        return $this->senha;
    }
    public function setSenha($value)
    {
        $this->senha = $value;
    }

// ==================================================================================================================================== //
    // ! Método - Função de Busca por ID - OK
    public function loadById($_id)
    {
        $sql = new Sql();
        $results = $sql->select("SELECT * FROM administrador WHERE id = :id",array(':id'=>$_id));
        if(count($results)>0)
        {
            $this->setData($results[0]);
        }
    }

    // ! Método - Função de  gerar lista - OK
    public static function getList()
    {
        $sql = new Sql();
        return $sql->select("SELECT * FROM administrador order by id");
    }

    // ! Método - Função de busca pelo nome do admin - OK
    public static function search($nome_adm)
    {
        $sql = new Sql();
        return $sql->select("SELECT * FROM administrador WHERE nome LIKE :nome", 
        array(":nome"=>"%".$nome_adm."%"));
    }

    // ! Método - Função de efetuar login - PENDENTE
    public function efetuarLogin($_login,$_senha)
    {
        $sql = new Sql();
        $senha_cript = md5($_senha);
        $results = $sql->select("SELECT * FROM administrador WHERE login = :login and senha = :senha",
        array(
        ':login'=>$_login,
        ':senha'=>$senha_cript)
        );
        
        if(count($results)>0)
        {
            $this->setData($results[0]);
        }
    }
    
    // ! Método - Função de inserção de novo administrador - PENDENTE
    public function insert()
    {
        $sql = new Sql();
        $results = $sql-> select("CALL sp_insert_administrador(:nome, :email, :login, :senha)",
        array(
        ":nome"=>$this->getNome(),
        ":email"=>$this->getEmail(),
        ":login"=>$this->getLogin(),
        ":senha"=>md5($this->getSenha())
        ));

        if (count($results)>0) 
        {
            $this->setData($results[0]);
        }

    }

    // ! Método - Função atualizar dados de um usuário específico.
    public function update($_id,$_nome, $_email, $_login)
    {
        $sql = new Sql();
        $sql->query("UPDATE administrador SET nome =:nome, email = :email, login = :login WHERE id = :id",
        array(
            ":id"->$_id,
            ":nome"->$_nome,
            ":login"->$_login,
            ":email"->$_email
        ));
    }

    // ! Método - função para deletar usuário.
    public function delete()
    {
        $sql = new Sql();
        $sql->query("DELETE FROM administrador WHERE id =:id",
        array(":id"=>$this->getId()));
    }

    // ! Método construtor
    public function _construct($_nome="",$_email="",$_login="",$_senha="")
    {
        $this->nome =$_nome;
        $this->email =$_email;
        $this->login =$_login;
        $this->senha =$_senha;
    }

    // ! Método - Data - MySQL/Tabela Administrador
    public function Data($data)
    {
        $this->setId($data['id']);
        $this->setNome($data['nome']);
        $this->setEmail($data['email']);
        $this->setLogin($data['login']);
        $this->setSenha($data['senha']);
    }

     // * O LIKE puxa pela letra que foi citada, se a letra for antes do % o LIKE vai puxar todos os nomes possíveis com aquela letra
    // * Se for após o % puxará apenas no final do nome.
    // * Se for nos dois lados, puxará tanto do final quanto do meio.
}
?>