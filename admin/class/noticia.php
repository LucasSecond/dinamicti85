<?php

class Noticia
{
// ! Atributos da classe Notícia
private $id;
private $titulo;
private $ImgNoticia;
private $VisitaNoticia;
private $DataNoticia;
private $NoticiaAtivo;
 
// ! Método de acesso - Getters and Setters - Notícia

// ! ID
public function getId()
{
    return $this->id;
}

public function setId($value)
{
    $this->id = $value;
}
 
// ! Titulo
public function getTitulo()
{
    return $this->titulo;
}
public function setTitulo($value)
{
    $this->titulo = $value;
}

// ! Imagem Noticia
public function getImgNoticia()
{
    return $this->ImgNoticia;
}
public function setImgNoticia($value)
{
    $this->ImgNoticia = $value;
}

// ! Visita Noticia
public function getVisitaNoticia()
{
    return $this->VisitaNoticia;
}
public function setVisitaNoticia($value)
{
    $this->VisitaNoticia = $value;
}
 
// ! Data Noticia
public function getDataNoticia()
{
    return $this->DataNoticia;
}

public function setDataNoticia($value)
{
    $this->DataNoticia = $value;
}

// ! Noticia Ativo
public function getNoticiaAtivo()
{
    return $this->NoticiaAtivo;
}

public function setNoticiaAtivo($value)
{
    $this->NoticiaAtivo = $value;
}

// ========================================================================================================

// ! Método - Função de Busca por ID
public function loadById($_id)
    {
        $sql = new Sql();
        $results = $sql->select("SELECT * FROM noticias WHERE id = :id",array(':id'=>$_id));
        if(count($results)>0)
        {
            $this->setData($results[0]);
        }
    }

// ! Método - Função gerar lista de Notícias
public static function getList()
{
    $sql = new Sql();
    return $sql->select("SELECT * FROM noticias order by nome");
}

// ! Método - Função de busca pelo título
public static function search($titulo_noticia)
    {
        $sql = new Sql();
        return $sql->select("SELECT * FROM noticias WHERE titulo_noticia LIKE :titulo", 
        array(":titulo"=>"%".$titulo_noticia."%"));
    }

// ! Método - Função de inserção de novo administrador
    public function insert()
    {
        $sql = new Sql();
        $results = $sql->select("CALL sp_adm_insert(:nome, :email, :login, :senha)",
        array(
            ":nome"=>$this->getNome(),
            ":email"=>$this->getEmail(),
            ":login"=>$this->getLogin(),
            ":senha"=>md5($this->getSenha())
        ));
        if(count($results)>0)
        {
            $this->setData($results[0]);
        }
    }

// ! Método - Função de update uma notícia específica

// ! Método - Função deletar uma notícia específica

// ! Método - Data

// ! Método - Construtor

}
?>