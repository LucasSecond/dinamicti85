<?php

class Categoria
{

// ! Atributos da classe Categoria
    private $id;
    private $categoria;
    private $cat_ativo;

    // ===================================================

// ! Métodos de acesso - Getters and Setters - Categoria

    // ! ID
    public function getId()
    {
        return $this->id;
    }

    public function setId($value)
    {
        $this->id = $value;
    }


    // ! Nome - Categoria
    public function getCategoria()
    {
        return $this->categoria;
    }

    public function setCategoria($value)
    {
        $this->categoria = $value;
    }


    // ! Ativo 
    public function getCatAtivo()
    {
        return $this->cat_ativo;
    }

    public function setCatAtivo($value)
    {
        $this->cat_ativo = $value;
    }

// ==================================================================================================================================== //

// ! Método - Função de busca por ID
    public function loadById ($_id,$_categoria)
    {
        $sql = new Sql();
        $results = $sql->select("SELECT * FROM categoria where id = :id",
        array(":categoria"=>"%".$_categoria."%"));
        if(count($results)>0)
        {
            $this->setData($results[0]);
        }
    }

// ! Método - Função de busca pelo nome (Categoria)
    public function search($categoria)
    {
        $sql = new Sql();
        return $sql->select("SELECT * FROM categoria WHERE nome LIKE :categoria",
        array(
            ":categoria"=>"%".$categoria."%"));
    }

// ! Método - Função gerar lista de Categorias
public static function getList()
{
    $sql = new Sql();
    return $sql->select("SELECT * FROM categoria order by nome");
}

// ! Método - Função inserir categoria
    public function insert()
    {
        $sql = new Sql();
        $results = $sql->select("CALL sp_categoria_insert(:categoria, :cat_ativo)",
        array(
            ":categoria"=>$this->getCategoria(),
            ":cat_ativo"=>$this->getCatAtivo()
        ));
        if(count($results)>0)
        {
            $this->setData($results[0]);
        }
    }

// ! Método - Função alterar/update categoria
    public function update($_categoria, $cat_ativo)
    {
        $sql = new Sql();
        $sql->query("UPDATE categoria SET categoria =:categoria, cat_ativo = :cat_ativo WHERE id = :id");
        array(
             ":categoria"->$_categoria,
             ":cat_ativo"->$cat_ativo
             );
    }

// ! Método - Função deletar categoria
    public function delete()
    {
        $sql = new Sql();
        $sql->query("DELETE FROM categoria WHERE id = :id",
        array(":id"=>$this->getId())
    );
    }

// ! Método - SetData
    public function Data($data)
    {
        $this->setId($data['categoria']);
        $this->setId($data['cat_ativo']);
    }

// ! Método construtor
    public function _construct($_categoria="",$_cat_ativo="")
    {
        $this->categoria =$_categoria;
        $this->cat_ativo =$_cat_ativo;
    }

}
?>  