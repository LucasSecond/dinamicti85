<?php
// include_once('../../config.php');

class Sql extends PDO
{
    private $cn;
    public function __construct()
    {
        $this->cn = new PDO("mysql:host=127.0.0.1;dbname=dinamico85db","root","");
    }

    // ! Método - atribui parâmetros para uma query sql
    public function setParams($comando, $parametros = array())
    {
        foreach($parametros as $key => $value)
         {
             $this->setParam($comando, $key, $value);
         }
    }

    // * = (atribuição) | == (comparação) | === (comparação absoluta)
    // * => (assosiação) | -> (propriedade)
    
    public function setParam($cmd, $key, $value)
    {
        $cmd->bindParam($key, $value);
    }

    public function query($comandoSql, $params = array())
    {
        $cmd = $this->cn->prepare($comandoSql);
        $this->setParams($cmd, $params);
        $cmd->execute();
        return $cmd;
    }

    // * A função select faz uma leitura do Params, depois o Param 
    // * e assim em diante um loop infinito
    // * Chegando na função select, o método te entrega o resultado.
    
    public function select($comandoSql, $params = array())
    {
        $cmd = $this->query($comandoSql, $params);
        return $cmd->fetchAll(PDO::FETCH_ASSOC);
    }
}

?>