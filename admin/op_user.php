<?php
require_once('../config.php');

// SESSÃO
if(isset($_SESSION['logado']))
{
    if($_SESSION['logado'])
    {
        header('Location: index.php');
    }
}

// Inserir usuário - TESTAR
if (isset($_POST['cadastro_user']))
{
    $user = new Usuario(
        $_POST['nome_user'],
        $_POST['email_user'],
        $_POST['login_user'],
        $_POST['senha_user'],
        $_POST['foto_user']
    );
    
    if($user->getId()!=null)
    {
        $user->insert();
        header('location:../../index.php?&msg=ok');
    }
    else
    {
        header('location:../../admin/user/frm_user.php?&msg=erro');
    }

    // Efetuar login do usuário
    if(isset($_POST['logar_user']))
    {
      $login_user = isset($_POST['login_user'])?$_POST['login_user']:'';
      $senha_user = isset($_POST['senha_user'])?$_POST['senha_user']:'';

      if(empty($login_user) || empty($senha_user))
      {
        // echo 'preencha as informações de usuário'
        header('Location: index.php?msg=Preencha os dados de Usuário');
        exit;
      }
        $user = new Usuario();
        $user->efetuarLogin($login_user,$senha_user);
        if (($user->getId()==null)) 
        {
            echo $user->getId();
            //echo 'Usuário ou Senha incorretos';
            header('Location: index.php?msg=Usuário ou Senha incorretos');
            exit;
        }
    }

    // registrando sessão do usuário
    $_SESSION['logado'] = true;
    $_SESSION['id_user'] = $user->getId();
    $_SESSION['nome_user'] = $user->getNome();
    $_SESSION['login_user'] = $user->getLogin();

    // Encerrando sessão de usuário
        if($_GET['sair'])
      {
        $_SESSION['false'] = false;
        $_SESSION['id_user'] = null;
        $_SESSION['nome_user'] = null;
        $_SESSION['login_user'] = null;
      }
}
?>