<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Novo Administrador</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div id="formulario-menor">
        <form action="op_administrador.php" method="post">

            <fieldset>

                <input type="hidden" id="id_adm" name="id">

                <legend>Inserir Novo Administrador</legend>

                <label for="">Nome</label>
                <input type="text" name="nome_adm" required>
                <p>
                <label for="">Email</label>
                <input type="text" name="email_adm" required>
                <p>
                <label for="">Login</label>
                <input type="text" name="login_adm" required>
                <p>
                <label for="">Senha</label>
                <input type="password" name="senha_adm" required>
                <p>
                <label for="">Confirma Senha</label>
                <input type="password" name="confirma_senha_adm" required>
                <br>
                <input type="submit" name="btn_cadastro" value="Cadastrar" class="botao">

            </fieldset>

        </form>
    </div>
</body>
</html>
