<?php
if (isset($_POST['txt_noticia']))
    {
    require_once('conexao.php');

    $categoria = $_POST['txt_noticia'];
    $ativo = isset($_POST['check_ativo'])?'1':'0';

    $cmd = $cn->prepare("INSERT INTO noticias (titulo_noticias, img_noticias, data_noticia, noticia_ativo, noticia) 
    VALUES (:tit_noticia, :img_noticia, :dat_noticia, :not_ativo, :noticia )");
    $cmd->execute(
    array(
        ':cat'=>$categoria,
        ':ativ'=>$ativo
        
    ));
    header('location:principal.php?link=2&msg=ok');
}
?>