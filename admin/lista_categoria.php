<?php
require_once('conexao.php');
$query = "select * from categoria";
$cmd = $cn->prepare($query);
$cmd->execute();
$categorias_retornadas = $cmd->fetchAll(PDO::FETCH_ASSOC);
if(count($categorias_retornadas)>0) 
 {
    print_r($categorias_retornadas);
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <title>Lista Categoria</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    
    <table id="tb_categoria" width="100%" border="0" cellpadding="1" bgcolor="#fff">
        <tr bgcolor="#993300" align="center">
            <th width="15%" height="2"><font size="2" color="#fff">Código</font></th>
            <th width="57%" height="2"><font size="2" color="#fff">Categoria</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Ativo</font></th>
            <th colspan="2"><font size="2" color="#fff">Opções</font></th>
        </tr>

    <?php
        foreach($categorias_retornadas as $categoria) {
    ?>

        <tr align="center">
            <td><font size="2" face="verdana, arial"color="#000">
                <?php echo $categoria['id_categoria']; ?></font></td>

            <td><font size="2" face="verdana, arial"color="#000">
                <?php echo $categoria['categoria']; ?></font></td>

            <td><font size="2" face="verdana, arial"color="#000">
                <?php echo $categoria['cat_ativo']; ?></font></td>
                
            <td align="center"><font size="2" face="verdana, arial" color="#fff"><a href="principal.php">Alterar</a></font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#fff"><a href="principal.php">Excluir</a></font></td>
        </tr>
<?php }} ?>
</table>

</body>
</html>