<?php
$id     = filter_input(INPUT_GET,'id');
$nome   = filter_input(INPUT_GET,'nome');
$email  = filter_input(INPUT_GET,'email');
$login  = filter_input(INPUT_GET,'login');
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <title>Alteração de Administrador - <?php $_SESSION['user']?></title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <form action="op_administrador.php" method="POST" enctype="multipart/form-data"></form>
    <fieldset>

        <legend>Alteração de Administrador</legend>

        <input type="hidden" name="id_adm" value="<?php echo $id; ?>">

        <div>
            <label for="">Nome</label>
            <input type="text" name="nome_adm" value="<?php echo $nome; ?>">
       </div>

        <div>
            <label for="">E-mail</label>
            <input type="text" name="email_adm" value="<?php echo $email; ?>">
       </div>

        <div>
            <label for="">Login</label>
            <input type="text" name="login_adm" value="<?php echo $login; ?>">
       </div>

        <div>
             <input type="submit" name="alterar_adm" value="Alterar">
        </div>

    </fieldset>
</body>
</html>