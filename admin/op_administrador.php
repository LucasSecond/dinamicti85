<?php
require_once('../config.php');

// ! Inserir Administrador
// * O "btn_cadastrar_adm" é o botão de cadastrar o usuário, 
// * após isso é a sequência de ações que irá ocorrer a inserção de dados no Mysql.
// var_dump($_POST);
// if (isset($_POST['btn_cadastro']))
// {
//     // * A classe administrador pega os dados inserido no formulário (POST) 
//     // * e com base nos dados irá para a próxima etapa.
//     $admInsert = new Administrador
//     (
//         $_POST['nome_adm'],
//         $_POST['email_adm'],
//         $_POST['login_adm'],
//         $_POST['senha_adm']
//     );
//     $admInsert->insert(); 
//     if($admInsert->getId()>0)
//     {
//        header('location:principal.php?link=11&msg=ok'); 
//     }  
// }

// ! Excluindo/deletando administrador

    // $id= filter_input(INPUT_GET,'id');
    // $excluir = filter_input(INPUT_GET,'excluir');

    // // A classe administrador está identificada como $admins, o usuário irá selecionar o ID com o setid e após isso irá executar o método Delete da classe Administrador 
    // //e após excluir o usuário será redirecioinado para tela principal.
    // if(isset($id)&& $excluir==1)
    // {
    //     $admin = new Administrador();
    //     $admin = setId($id);
    //     $admin->delete();
    //     header('location:principal.php?link=10&msg=ok');
    // }

// ! Alterar dados de um administrador
    // O botão "alterar_adm" está em alterar_administrador.php que é na verdade atualização de dados de um administrador específico,
    // identificado como $adm vai puxar diretamente os dados re-inseridos no formulário do update e com base nisso, irá atualizar na tabela
    // administrador.
    // if(isset($_POST['alterar_adm']))
    // {
    //     $adm = new Administrador();
    //     $adm->update($_POST['id'], $_POST['nome_adm'], $_POST['email_adm'], $_POST['login_adm']);
    //     header('location:principal.php?link=10&msg=ok');
    // }

// ! Efetuar login - 2
    //  $adm_login = new Administrador();

    //  var_dump($_POST);
    //  if(isset($_POST['btn_logar_adm']) && isset($_POST['txt_login_adm']))
    // {
    //     $adm_login->efetuarlogin(
    //         $_POST['txt_login_adm'],
    //         $_POST['txt_senha_adm']
    //     );

    //  if($adm_login->getId()>0)
    //     {
    //         header("Location:principal.php");
    //     }
    //  else
    //     {
    //         header("Location:index.php");
    //     }

    // }


    // ! Efetuar login do administrador
    if (isset($_POST['btn_logar_adm']))
    {
        // * Pega os dados inserido no POST da tela de login do Administrador.
        $txt_login_adm = isset($_POST['txt_login_adm'])?$_POST['txt_login_adm']:'';
        $txt_senha_adm = isset($_POST['txt_senha_adm'])?$_POST['txt_senha_adm']:'';

        // * Com os dados do POST a classe administrador checa diretamente com o método EfetuarLogin e valida os dados.
        $adm = new Administrador();        
        $adm->efetuarLogin(
            $_POST['txt_login_adm'],
            $_POST['txt_senha_adm']);        
        if ($adm->getId()>0)

        // * Se caso os dadoa estiverem incorretos ou faltarem dados, o usuário será impedido 
        // *de ir adiante para a página PRINCIPAL.
        // * Caso o usuário esquecer a parte de login ou senha, irá aparecer uma notificação 
        // * falando para preencher os dados.

        if (!empty($_POST) AND (empty($_POST['txt_login_adm']) OR empty($_POST['txt_senha_adm']))) 
        {
            header("Location: index.php?msg=Preencha login e senha"); 
            exit;
        }

        // * Se o usuário for cadastrado e os dados estiverem corretos, o método getId vai puxar o ID do usuário/administrador direto do MySql
        // * Caso contrário, o usuário não existe ou as informações estão incorretas.
        if($adm->getId() == 0)
        {
            header('location:index.php?msg=Usuario ou senha incorretos');
            exit;
        }

        // SESSÃO - CONECTADO
    //     {            
    //         $_SESSION['logado'] = true;
    //         $_SESSION['id_adm '] = $adm->getId();
    //         $_SESSION['nome_adm'] = $adm->getNome();
    //         $_SESSION['login_adm'] = $adm->getLogin();
    //         $_SESSION['email_adm'] = $adm->getEmail();
    //         header('location:principal.php');
    //         exit;
    //     }

    // }

    //     //Encerrando sessão de usuário
    //     if($_GET['sair'])
    //     {
    //         $_SESSION['false'] = false;
    //         $_SESSION['id_adm'] = null;
    //         $_SESSION['nome_adm'] = null;
    //         $_SESSION['login_adm'] = null;
    //         //registro de logs
    //         header('Location: index.php');
    //     }

?>