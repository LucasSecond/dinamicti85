<!-- <?php
// require_once('conexao.php');
// $query = "select * from administrador";
// $cmd = $cn->prepare($query);
// $cmd->execute();
// $adm_retornado = $cmd->fetchAll(PDO::FETCH_ASSOC);
// if(count($adm_retornado)>0) {
//     print_r($adm_retornado);
?> -->

<!DOCTYPE html>
<html lang="pt-BR">

    <head>
        <meta charset="UTF-8">
        <title>Lista Administrador</title>
        <link rel="stylesheet" href="css/style.css">
    </head>

    <body>
      <table id="tb_administrador" width="100%" border="0" cellpadding="1" bgcolor="#fff">
            <tr bgcolor="#993300" align="center">
                <th width="10%" height="2"><font size="2" color="#fff">ID</font></th>
                <th width="50%" height="2"><font size="2" color="#fff">Nome</font></th>
                <th width="25%" height="2"><font size="2" color="#fff">Email</font></th>
                <th width="25%" height="2"><font size="2" color="#fff">Login</font></th>
                <th colspan="2"><font size="2" color="#fff">Opções</font></th>
            </tr>

         <?php
            require_once('../config.php');
            $admins = Administrador::getList();
            foreach($admins as $adm)
          {
         ?>

         <tr align="center">
            <td><font size="2" face="verdana, arial"color="#000"><?php echo $adm['id']; ?></font></td>
            <td><font size="2" face="verdana, arial"color="#000"><?php echo $adm['nome']; ?></font></td>
            <td><font size="2" face="verdana, arial"color="#000"><?php echo $adm['email']; ?></font></td>
            <td><font size="2" face="verdana, arial"color="#000"><?php echo $adm['login']; ?></font></td>

            <td align="center"><font size="2" face="verdana, arial" color="#fff">
                <a href="<?php echo "alterar_administrador.php?
                id=".$adm['id'].
                "&nome=".$adm['nome_adm'].
                "&email=".$adm['email_adm'].
                "&login=".$adm['login_adm'];?>">Alterar</a>
            </font></td>

            <td align="center"><font size="2" face="verdana, arial" color="#fff">
                <a href="<?php echo "op_administrador.php?excluir=1&id".$adm['id'];?>">Excluir</a>
            </font></td>
            
        </tr> 
        <?php } ?>
     </table>
    </body>
</html>