<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Área Administrativa - Sistema Dinâmico</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div id="principal">
        <div id="cabecalho">
            <div id="titulo_topo">
                <img src="img/a-admin.png" alt="">
                <br>
                <p>(<a href="index.php">Logout</a>)</p>
            </div>
        </div> <!-- final cabeçalho -->
        <div id="corpo">
            <div id="esquerdo">
                <div id="sessao"><b>Categoria</b>
                    <ul>
                        <li><a href="principal.php?link=2">Cadastrar categoria</a></li>
                        <li><a href="principal.php?link=3">Lista de Categorias</a></li>
                    </ul>
                </div>
                <div id="sessao"><b>Post</b>
                        <ul>
                            <li><a href="principal.php?link=4">Cadastrar post</a></li>
                            <li><a href="principal.php?link=5">Lista de Posts</a></li>
                        </ul>
                </div>    
                <div id="sessao"><b>Noticia</b>
                        <ul>
                            <li><a href="principal.php?link=6">Cadastrar notícia</a></li>
                            <li><a href="principal.php?link=7">Lista de Noticias</a></li>
                        </ul>
                </div>
                <div id="sessao"><b>Banner</b>
                        <ul>
                            <li><a href="principal.php?link=8">Cadastrar banner</a></li>
                            <li><a href="principal.php?link=9">Lista de Banners</a></li>
                        </ul>
                </div>  
                <div id="sessao"><b>Administrador</b>
                        <ul>
                            <li><a href="principal.php?link=10">Cadastrar Administrador</a></li>
                            <li><a href="principal.php?link=11">Lista de Administradores</a></li>
                        </ul>
                </div>
                <div id="sessao"><b>Usuário</b>
                        <ul>
                            <li><a href="principal.php?link=11">Cadastrar Usuário</a></li>
                            <li><a href="principal.php?link=12">Lista de usuários</a></li>
                        </ul>
                </div>

            </div> <!-- Final do esquerdo -->
            <div id="direito">
                <?php
                    if(isset($_GET['link'])){
                        $link = $_GET['link'];

                        // ! Principal
                        $pag[1]="index.php";
                        // ! Categoria
                        $pag[2]="frm_categoria.php";
                        $pag[3]="lista_categoria.php";

                        // ! Post
                        $pag[4]="frm_post.php";
                        $pag[5]="lista_post.php";

                        // ! Notícia
                        $pag[6]="frm_noticia.php";
                        $pag[7]="lista_noticia.php";

                        // ! Banner
                        $pag[8]="frm_banner.php";
                        $pag[9]="lista_banner.php";

                        // ! Administrador
                        $pag[10]="frm_administrador.php";
                        $pag[11]="lista_administrador.php";

                        // ! Usuário
                        $pag[12]="frm_usuario.php";
                        $pag[13]="lista_usuario.php";

                        // ! Usuário - Cadastro
                        $pag[14]="login.php"; // Tela de login
                        $pag[15]="lista_usuario.php"; // PENDENTE
                        if(!empty($link)){
                            if(file_exists($pag[$link])){
                                include $pag[$link];
                            }
                            else{
                                include $pag[1];
                            }
                        }
                    }
                    else{
                        include "home.php";
                    }
                ?>
            </div> <!-- Final do direito -->
        </div>
    </div>
    
</body>
</html>